//
//  GropuCategoriesCollectionViewCell.swift
//  Reedius
//
//  Created by Sushant Jugran on 25/11/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit

class GroupCategoriesCollectionViewCell: UICollectionViewCell {
    
    //================
    //MARK:-IBOutlets
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categoryTitle: UILabel!
}
