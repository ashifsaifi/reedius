//
//  HomeConnectTableViewCell.swift
//  Reedius
//
//  Created by Jyoti Negi on 04/12/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class HomeConnectTableViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var shadowView:CustomView!
    @IBOutlet weak var name:UILabel!
    @IBOutlet weak var img:UIImageView!
    @IBOutlet weak var date:UILabel!
    @IBOutlet weak var type:UILabel!
    @IBOutlet weak var post:UILabel!
    @IBOutlet weak var like:UILabel!
    @IBOutlet weak var comment:UILabel!
    
    var data:[EOPostImages]?
    override func awakeFromNib() {
        super.awakeFromNib()
        shadowView.shadowFill()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "ConnectCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ConnectCollectionViewCell")
    }
}
extension HomeConnectTableViewCell :UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ConnectCollectionViewCell", for: indexPath) as! ConnectCollectionViewCell
        
        let obj = "https://s3.ap-south-1.amazonaws.com/reedius/post/" + (data?[indexPath.row].image_name ?? "")
        if  obj  != "" {
            Alamofire.request(obj ?? "").responseImage { (image) in
                if let image = image.result.value{
                    cell.collImg.image = image
                }
            }
        }
        return cell
    }
    
}
