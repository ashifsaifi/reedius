//
//  GroupSuggestionsTableViewCell.swift
//  Reedius
//
//  Created by Sushant Jugran on 25/11/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit

class GroupSuggestionsTableViewCell: UITableViewCell {

    //======================
    //MARK:- IBOutlets
    
    @IBOutlet weak var categoryImageView: UIImageView!
    
    //=====================
    //MARK:- Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
