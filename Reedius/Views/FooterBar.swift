//
//  FooterBar.swift
//  Reedius
//
//  Created by Sushant Jugran on 27/11/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit

class FooterBar : UITableViewCell{
    
    //===================
    //MARK:- IBOutlets
    
    
    @IBOutlet weak var messageImageView : UIImageView!
    @IBOutlet weak var offerImageView   : UIImageView!
    @IBOutlet weak var groupImageView   : UIImageView!
    @IBOutlet weak var homeImageView    : UIImageView!
    @IBOutlet weak var messageLabel     : UILabel!
    @IBOutlet weak var offerLabel       : UILabel!
    @IBOutlet weak var groupLabel       : UILabel!
    @IBOutlet weak var homeLabel        : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setSelecetedFooterBarItem()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    @IBAction func groupButtonTapped(_ sender: Any) {
        AppHelper.getInstance.setSelectedTabBarItem(item: 4)
        setSelecetedFooterBarItem()
         AppHelper.getInstance.pushViewController(identifier: "ExploreViewController", controller: appDelegate().controller!, animation: false)
        
    }
    
    @IBAction func offerButtonTapped(_ sender: Any) {
        AppHelper.getInstance.setSelectedTabBarItem(item: 3)
        setSelecetedFooterBarItem()
         AppHelper.getInstance.pushViewController(identifier: "OffersViewController", controller: appDelegate().controller!, animation: false)
        
    }
    
    @IBAction func AddButtonTapped(_ sender: Any) {
        AppHelper.getInstance.setSelectedTabBarItem(item: 2)
        setSelecetedFooterBarItem()
         AppHelper.getInstance.pushViewController(identifier: "AddViewController", controller: appDelegate().controller!, animation: false)
    }
    
    @IBAction func messageButtonTapped(_ sender: Any) {
        AppHelper.getInstance.setSelectedTabBarItem(item: 1)
        setSelecetedFooterBarItem()
        AppHelper.getInstance.pushViewController(identifier: "MessageViewController", controller: appDelegate().controller!, animation: false)
    }
    
    @IBAction func homeButtonTapped(_ sender: Any) {
        AppHelper.getInstance.setSelectedTabBarItem(item: 0)
       
        setSelecetedFooterBarItem()

        AppHelper.getInstance.pushViewController(identifier: "HomeViewController", controller: appDelegate().controller!, animation: false)
        
    }
    
    func setSelecetedFooterBarItem() {
        var item = AppHelper.getInstance.getSelectedTabBarItem()
            switch item {
            case 0 : homeLabel.textColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
                    messageLabel.textColor = #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1)
                     offerLabel.textColor = #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1)
                groupLabel.textColor = #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1)
            
            homeImageView.setImageColor(color: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1))
            messageImageView.setImageColor(color: #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1))
                offerImageView.setImageColor(color: #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1))
                groupImageView.setImageColor(color: #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1))
            case 1 : messageLabel.textColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
            homeLabel.textColor = #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1)
            offerLabel.textColor = #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1)
            groupLabel.textColor = #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1)
            homeImageView.setImageColor(color: #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1))
            messageImageView.setImageColor(color: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1))
            offerImageView.setImageColor(color: #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1))
            groupImageView.setImageColor(color: #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1))
            case 2 : print("2")
            case 3: offerLabel.textColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
            messageLabel.textColor = #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1)
            homeLabel.textColor = #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1)
            groupLabel.textColor = #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1)
            homeImageView.setImageColor(color: #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1))
            messageImageView.setImageColor(color: #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1))
            offerImageView.setImageColor(color: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1))
            groupImageView.setImageColor(color: #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1))
            case 4: groupLabel.textColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
            offerLabel.textColor = #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1)
            messageLabel.textColor = #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1)
            homeLabel.textColor = #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1)
            homeImageView.setImageColor(color: #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1))
            messageImageView.setImageColor(color: #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1))
            offerImageView.setImageColor(color: #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1))
            groupImageView.setImageColor(color: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1))
            default :
                print("default item")
            }
        
    }
    func appDelegate () -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
}
