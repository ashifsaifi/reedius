//
//  HomeGroupCollectionViewCell.swift
//  Reedius
//
//  Created by Jyoti Negi on 04/12/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit

class HomeGroupCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var name :UILabel!
    @IBOutlet weak var img:UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
