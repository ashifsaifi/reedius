//
//  HomeGroupTableViewCell.swift
//  Reedius
//
//  Created by Jyoti Negi on 04/12/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class HomeGroupTableViewCell: UITableViewCell {
    
    var data : [EOAllGroup]?
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewAllView: CustomView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "HomeGroupCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeGroupCollectionViewCell")
        viewAllView.shadowFill()
//        let nib = UINib(nibName: "HomeGroupCollectionViewCell", bundle: nil)
//        collectionView?.register(nib, forCellWithReuseIdentifier: "HomeGroupCollectionViewCell")
    }
}
extension HomeGroupTableViewCell :UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data!.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 90)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeGroupCollectionViewCell", for: indexPath) as! HomeGroupCollectionViewCell
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.lightGray.cgColor
        let obj = data?[indexPath.item]
        cell.name.text = obj?.group_name
      
        
        if obj?.img != "" {
            
            Alamofire.request(obj?.img ?? "").responseImage { (image) in
                if let image = image.result.value {
                    cell.img.image = image
                    
                }
                
            }
        }
        
        
        return cell
    }
    
    
}
