//
//  HomeOfferTableViewCell.swift
//  Reedius
//
//  Created by Jyoti Negi on 04/12/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit

class HomeOfferTableViewCell: UITableViewCell {
    @IBOutlet weak var shadowView: CustomView!
    @IBOutlet weak var advertiseImage:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        shadowView.shadowFill()
        
    }
    
    
    
}
