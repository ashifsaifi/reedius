//
//  requestModel.swift
//  Reedius
//
//  Created by Sushant Jugran on 25/11/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import Foundation

class  LoginModel {
    var addressLine1H = ""
    var addressLine1W = ""
    var addressLine2H = ""
    var addressLine2W = ""
    var age = ""
    var cityH = ""
    var cityW = ""
    var countryH = ""
    var countryW = ""
    var createdDate = ""
    var email = ""
    var groupId = ""
    var latH = ""
    var latW = ""
    var longH = ""
    var longW = ""
    var mobile = ""
    var name = ""
    var otp = ""
    var password = ""
    var role = ""
    var sex = ""
    var socilaToken = ""
    var socialType = ""
    var stateH = ""
    var stateW = ""
    var userId = ""
    var userimg = ""
    var waitOtp = ""
    init?(dict: [String : Any]){
        self.addressLine1H = dict["address_line1_h"] as? String ?? ""
        self.addressLine1W = dict["address_line1_w"] as? String ?? ""
        self.addressLine2H = dict["address_line2_h"] as? String ?? ""
        self.addressLine2W = dict["address_line2_w"] as? String ?? ""
        self.age = dict["age"] as? String ?? ""
        self.cityH = dict["city_h"] as? String ?? ""
        self.cityW = dict["city_w"] as? String ?? ""
        self.countryH = dict["country_h"] as? String ?? ""
        self.countryW = dict["created_date"] as? String ?? ""
        self.createdDate = dict["created_date"] as? String ?? ""
        self.email = dict["email"] as? String ?? ""
        self.groupId = dict["group_id"] as? String ?? ""
        self.latH = dict["lat_h"] as? String ?? ""
        self.latW = dict["lat_w"] as? String ?? ""
        self.longH = dict["long_h"] as? String ?? ""
        self.longW = dict["long_w"] as? String ?? ""
        self.mobile = dict["mobile"] as? String ?? ""
        self.name = dict["name"] as? String ?? ""
        self.otp = dict["otp"] as? String ?? ""
        self.password = dict["password"] as? String ?? ""
        self.role = dict["role"] as? String ?? ""
        self.sex = dict["sex"] as? String ?? ""
        self.socilaToken = dict["social_token"] as? String ?? ""
        self.socialType = dict["social_type"] as? String ?? ""
        self.stateH = dict["state_h"] as? String ?? ""
        self.stateW = dict["state_w"] as? String ?? ""
        self.userId = dict["user_id"] as? String ?? ""
        self.userimg = dict["user_img"] as? String ?? ""
        self.waitOtp = dict["wait_otp"] as? String ?? ""
    }
}

