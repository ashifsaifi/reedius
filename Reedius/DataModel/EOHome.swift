//
//  EOHome.swift
//  Reedius
//
//  Created by Jyoti Negi on 09/12/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit

class EOHome: Codable {
    var post_id :Int?
    var user_id: Int?
    var group_id: Int?
    var type: String?
    var is_public: Int?
    var post: String?
    var created_date: String?
    var post_like: Int?   
    var comment: Int?
    var latitude: String?
    var longitude: String?
    var location: String?
    var post_range: Int?
    var poll_duration: Int?
    var is_anonymous: Int?
    var is_reported: Int?
    var report_id: Int?
    var status: Int?
    var created_at: String?
    var updated_at: String?
    var deleted_at: String?
    var name:String?
    var user_pic: String?
    var post_images: [EOPostImages]?
    var poll_options: [EoPollOptions]?
    var is_liked: Int?
    var is_voted: Int?
    var option_selected: Int?
    var business_id: Int?
    var add_title: String?
    var add_description: String?
    var add_banner: String?
}

class EoPollOptions:Codable{

    var option_id: String?
    var option: String?
    var count: Int?
    
}
class EOPostImages :Codable{
    var image_id : String?
    var image_name :String?
}
