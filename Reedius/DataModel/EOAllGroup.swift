//
//  EOAllGroup.swift
//  Reedius
//
//  Created by Jyoti Negi on 09/12/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit

class EOAllGroup: Codable {
    var group_id: Int?
    var group_name: String?
    var img: String?
}
