//
//  Extensions.swift
//  Reedius
//
//  Created by Sushant Jugran on 25/11/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    var showToastTag :Int {return 999}
    
    //Generic Show toast
    func showToast(message : String, duration:TimeInterval) {
        
        let toastLabel = UILabel(frame: CGRect(x:30, y:UIScreen.main.bounds.size.height - 100, width: (self.frame.size.width)-60, height:30))
        
        toastLabel.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.8967519264)
        toastLabel.textColor = UIColor.white
        toastLabel.numberOfLines = 0
        toastLabel.layer.borderColor = UIColor.lightGray.cgColor
        toastLabel.layer.borderWidth = 1.0
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "HelveticaNeue", size: 17.0)
        toastLabel.text = message
        toastLabel.isEnabled = true
        toastLabel.alpha = 0.70
        toastLabel.tag = showToastTag
        toastLabel.layer.cornerRadius = 15;
        toastLabel.clipsToBounds  =  true
        self.addSubview(toastLabel)
        
        UIView.animate(withDuration: duration, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.95
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    //Generic Hide toast
    func hideToast(){
        if let view = self.viewWithTag(self.showToastTag){
            view.removeFromSuperview()
        }
    }
    
}
extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}
