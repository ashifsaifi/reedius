//
//  AppConstants.swift
//  Reedius
//
//  Created by Sushant Jugran on 25/11/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import Foundation

struct AppConstansts {
    struct AppUrls {
        static let baseUrl = "http://admin.reedius.com/"
        static let loginApp = "login_app"
        static let verifyOtp = "verify_otp"
        static let signup = "reg_with_app"
    }
    struct loginParameters {
        static let emailId = "email"
        static let password = "password"
    }
    struct ErrorMessages {
        static let internetConnection = "Please Connect to Internet"
    
    }
    struct  dataKeys {
        static let data = "data"
        static let message = "message"
        static let statusCode = "status_code"
    }
    struct userData {
        static let addressLine1h = "address_line1_h"
        static let addressLine1W = "address_line1_w"
        static let addressLine2H =  "address_line2_h"
        static let addressLine2W = "address_line2_w"
        static let age = "age"
        static let cityH = "city_h"
        static let cityW = "city_w"
        static let countryH = "country_h"
        static let createdDate = "created_date"
        static let email = "email"
        static let groupId = "group_id"
        static let latH = "lat_h"
        static let latW = "lat_w"
        static let longH = "long_h"
        static let longW = "long_w"
        static let mobile = "mobile"
        static let name = "name"
        static let otp = "otp"
        static let password = "password"
        static let role = "role"
        static let sex = "sex"
        static let socialToken = "social_token"
        static let socialType = "social_type"
        static let stateH = "state_h"
        static let stateW = "state_w"
        static let userId = "user_id"
        static let userImg = "user_img"
        static let waitOtp = "wait_otp"
    }
    struct userDefaults {
        static let addressLine1h = "address_line1_h"
        static let addressLine1W = "address_line1_w"
        static let addressLine2H =  "address_line2_h"
        static let addressLine2W = "address_line2_w"
        static let age = "age"
        static let cityH = "city_h"
        static let cityW = "city_w"
        static let countryH = "country_h"
        static let countryW = "country_w"
        static let createdDate = "created_date"
        static let email = "email"
        static let groupId = "group_id"
        static let latH = "lat_h"
        static let latW = "lat_w"
        static let longH = "long_h"
        static let longW = "long_w"
        static let mobile = "mobile"
        static let name = "name"
        static let otp = "otp"
        static let password = "password"
        static let role = "role"
        static let sex = "sex"
        static let socialToken = "social_token"
        static let socialType = "social_type"
        static let stateH = "state_h"
        static let stateW = "state_w"
        static let userId = "user_id"
        static let userImg = "user_img"
        static let waitOtp = "wait_otp"
        static let isUserLogin = "userLogin"
    }
    struct otpParameters {
        static let otp = "otp"
        static let userId = "user_id"
    }
    struct  signupParameters {
        static let email = "email"
        static let password = "password"
        static let name = "name"
        static let mobile = "mobile"
        static let age = "age"
        static let gender = "sex"
    }
}
var selectedTabBarItem = 0
