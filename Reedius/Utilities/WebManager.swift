//
//  WebManager.swift
//  Reedius
//
//  Created by Sushant Jugran on 25/11/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import Foundation
import Alamofire

class WebManager: NSObject {
    static let getInstance = WebManager()
    private override init() {
    }
    func requestWithGet(baseUrl: String, endUrl: String,onCompletion: (([String:Any]) -> Void)? = nil , onError: ((Error?) -> Void)? = nil){
        let url = baseUrl + endUrl
        Alamofire.request(url, method: .get ).responseJSON { response in
            
            switch response.result{
            case .success:
                onCompletion!(response.result.value as! [String:Any])
            case .failure:
                onError!( nil)
            }
        }
    }
    
    func requestWithPost(baseUrl: String, endUrl: String,parameters: [String:String], onCompletion: (([String:Any]) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        let url = baseUrl + endUrl
        
        Alamofire.request(url, method: .post, parameters: parameters, headers: nil)
            .responseJSON { response in
                switch response.result {
                case .success:
                    onCompletion!(response.result.value as! [String:Any])
                case .failure:
                    onError!( nil)
                }
        }
    }
    
    func requestWithPostHeader(baseUrl: String, endUrl: String,parameters: [String:Any],onCompletion: (([String:Any]) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        let url = baseUrl + endUrl
        //        let val = String(userId)
        let header: HTTPHeaders = [  //Header.contentType : contentType,
            // Header.userId : val,
            //Header.token : token
            :
        ]
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header)
            .responseJSON { response in
                switch response.result {
                case .success:
                    onCompletion!(response.result.value as! [String:Any])
                case .failure:
                    print(response.result.error as Any)
                    onError!( response.result.error )
                }
        }
    }
}

class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}


