//
//  AppHelper.swift
//  Reedius
//
//  Created by Sushant Jugran on 25/11/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit

class AppHelper: NSObject {
    static let getInstance = AppHelper()
    private override init() {
    }
    var userDefaults: UserDefaults! = nil
    func alertController(title: String, message : String , buttonTitle: String,controller: UIViewController,isPushViewController: Bool , pushController: String ){
        let alertBox = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if isPushViewController == true{
            alertBox.addAction(UIAlertAction(title: buttonTitle, style: .cancel, handler: { (nil) in
                AppHelper.getInstance.pushViewController(identifier: pushController, controller: controller, animation: true)
            }))
        } else {
            alertBox.addAction(UIAlertAction(title: buttonTitle, style: .cancel, handler: nil))
        }
        
        controller.present(alertBox, animated: true, completion: nil)
    }
    
    func pushViewController(identifier: String, controller: UIViewController ,animation : Bool){
        let dest = controller.storyboard?.instantiateViewController(withIdentifier: identifier)
        controller.navigationController?.pushViewController(dest!, animated: animation)
    }
    func popViewController(controller: UIViewController){
        controller.navigationController?.popViewController(animated: true)
    }
    func pushViewController(identifier: String, controller: UIViewController,data: Any){
//        let nextViewController: BaseViewController = GetViewControllerIntance(id: id, controllerName: controllerName) as! BaseViewController
//        nextViewController.data = data
//        (AppDelegate.getDelegate().window?.rootViewController as! UINavigationController).pushViewController(nextViewController, animated: true)
    }
    func GetViewControllerIntance(id: String ,controllerName : UIViewController) -> UIViewController {
        
        var controllersArray: [UIViewController] = (AppDelegate.getDelegate().window?.rootViewController as! UINavigationController).viewControllers
        var targetController: UIViewController!
        for controller in controllersArray {
            if(controller.restorationIdentifier! == id) {
                controllersArray.remove(at: controllersArray.firstIndex(of: controller)!)
                break;
            }
        }
        targetController = controllerName.storyboard?.instantiateViewController(withIdentifier: id)
        let modalStyle: UIModalTransitionStyle = UIModalTransitionStyle.coverVertical
        targetController.modalTransitionStyle = modalStyle
        return targetController!
    }
    func showProgressIndicator(view:UIView) {
        DispatchQueue.main.async {
            let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
            let backView : UIView = UIView()
            backView.frame = view.bounds
            backView.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.3)
            backView.tag = 700
            actInd.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            actInd.center = backView.center
            actInd.hidesWhenStopped = true
            actInd.style =
                UIActivityIndicatorView.Style.whiteLarge
            backView.addSubview(actInd)
            view.addSubview(backView)
            actInd.startAnimating()
        }
    }
    
    func hideProgressIndicator(view:UIView) {
        DispatchQueue.main.async {
            for subviews in view.subviews {
                if subviews.tag == 700 {
                    if let indicatorView = subviews.subviews[0] as? UIActivityIndicatorView {
                        indicatorView.stopAnimating()
                    }
                    subviews.removeFromSuperview()
                }
            }
        }
    }
   
    func showAlert(controller : UIViewController,message : String){
        let alertController = UIAlertController(title: "Reedius", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        controller.present(alertController, animated: true, completion: nil)
    }
    
    func setValueForUserDefault(value: Any, key:  String) {
        if(userDefaults == nil) {
            userDefaults = UserDefaults.init(suiteName: "Reedius")
        }
        userDefaults.set(value, forKey: key)
    }
    
    func getUserDefaultForKey(key: String) -> Any {
        if(userDefaults == nil) {
            userDefaults = UserDefaults.init(suiteName: "Reedius")
        }
        guard  let value = userDefaults.value(forKey: key) else {
            return ""
        }
        if value is String {
            return value as! String
        }
        else if value is Int {
            return (value as! Int).description
        }
        else {
            return value
        }
    }
    
    func removeUserDefaultForKey(key:String) {
        userDefaults.removeObject(forKey: key)
    }
    
    func setUserLoggedIn(value:Bool) {
        AppHelper.getInstance.setValueForUserDefault(value: value, key: AppConstansts.userDefaults.isUserLogin)
    }
    
    func getUserLoggedIn() -> Bool {
        if let value = AppHelper.getInstance.getUserDefaultForKey(key: AppConstansts.userDefaults.isUserLogin) as? Bool{
            return value
        }else {
            return false
        }
    }
    
    func setSelectedTabBarItem ( item : Int) {
        selectedTabBarItem = item
    }
    
    func getSelectedTabBarItem() -> Int {
        return selectedTabBarItem
    }
}
