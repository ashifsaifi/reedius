//
//  CustomClass.swift
//  Reedius
//
//  Created by Sushant Jugran on 22/11/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//


import Foundation
import UIKit

@IBDesignable
class CustomButton: UIButton{
    @IBInspectable
    public var cornerRadius: CGFloat = 2.0 {
        didSet {
            self.layer.cornerRadius = self.cornerRadius
        }
    }
    public func gradientFill(){
        let colorTop =  UIColor(red: 110.0/255.0, green: 20.0/255.0, blue: 4/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 189.0/255.0, green: 86.0/255.0, blue: 20.0/255.0, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        //        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.bounds
        gradientLayer.startPoint =  CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x:1.0, y:0.5)
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}

class CustomImage: UIImageView {
    
    @IBInspectable
    public var borderWidth: CGFloat = 1.0 {
        didSet{
            self.layer.borderWidth = self.borderWidth
        }
    }
    @IBInspectable
    public var borderColor: UIColor = UIColor.clear {
        didSet{
            self.layer.borderColor = self.borderColor.cgColor
        }
    }
}

class CustomView: UIView {
    @IBInspectable
    public var cornerRadius: CGFloat = 2.0 {
        didSet{
            self.layer.cornerRadius = self.cornerRadius
        }
    }
    @IBInspectable
    public var borderWidth: CGFloat = 1.0 {
        didSet{
            self.layer.borderWidth = self.borderWidth
        }
    }
    @IBInspectable
    public var borderColor: UIColor = UIColor.clear {
        didSet{
            self.layer.borderColor = self.borderColor.cgColor
        }
    }
    public func shadowFill(){
        self.layer.shadowColor   = UIColor.gray.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset  = CGSize(width: -3, height: 3)
        self.layer.shadowRadius  = 5
    }
}

extension UITextField{
    func isTextFieldEmpty(controller: UIViewController,message: String)-> Bool  {
        guard self.text != "" else {
            let alertBox = UIAlertController(title: "1 Book", message: message, preferredStyle: .alert)
            alertBox.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (nil) in
                self.becomeFirstResponder()
            }))
            controller.present(alertBox, animated: true, completion: nil)
            return true
        }
        return false
    }
}
/*extension UIImage {
 public class func gif(asset: String) -> UIImage? {
 if let asset = NSDataAsset(name: asset) {
 return UIImage.gif(data: asset.data)
 }
 return nil
 }
 
 }*/
class BaseViewController: UIViewController {
    
}
