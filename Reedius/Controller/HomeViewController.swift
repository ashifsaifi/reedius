//
//  HomeViewController.swift
//  Reedius
//
//  Created by Jyoti Negi on 04/12/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit
import  SideMenu
import  Alamofire
import  AlamofireImage


class HomeViewController: UIViewController {
    @IBOutlet weak  var tableView: UITableView!
    @IBOutlet weak var footerView: UIView!
    var eoHome = [EOHome]()
    var eoGroupArray = [EOAllGroup]()

    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate().controller = self
        tableView.register(UINib(nibName: "HomeGroupTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeGroupTableViewCell")
        tableView.register(UINib(nibName: "HomeConnectTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeConnectTableViewCell")
        tableView.register(UINib(nibName: "HomeOfferTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeOfferTableViewCell")
         tableView.register(UINib(nibName: "PollTableViewCell", bundle: nil), forCellReuseIdentifier: "PollTableViewCell")
        
        getGroupCatogory()
        getHomeData()
    }
   @objc func tapAction(_ sender:UITapGestureRecognizer){
    let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SeeAllViewController")as! SeeAllViewController
    vc.allGroupArray = eoGroupArray
    self.navigationController?.pushViewController(vc, animated: true)
    
    }
  func  getGroupCatogory(){
    guard let url = URL(string: "http://admin.reedius.com/all_group_app")else{
       print("No data found")
        return
    }
    Alamofire.request(url, method: .post, parameters: [:], headers: nil).responseJSON { (response) in
        guard response.result.isSuccess else{
            print("error while fetching data \(response.result.error)")
            return
        }
        if let value = response.result.value  as? [String:Any]{
            print(value)
            do{
            let decoder = JSONDecoder()
            //        self.eoGroupArray = decoder.decode([EOAllGroup].self, from: JSONSerialization.data(withJSONObject: value(forKey: "data") as? [[String:Any]], options: []))
            self.eoGroupArray = try decoder.decode([EOAllGroup].self, from: JSONSerialization.data(withJSONObject: value["data"] as? [[String:Any]], options: []))
                self.tableView.reloadData()
            }catch let err{
                print(err)
                
                
            }
        }

    }
    }
    func getHomeData(){
        guard  let url = URL(string: "http://admin.reedius.com/view_all_post") else {
            print("no data")
            return
        }
        Alamofire.request(url, method: .post, parameters: ["user_id":119],  headers: nil).responseJSON { (response ) in
            guard response.result.isSuccess else {
                print("Error while fetching remote rooms:" + "\(response.result.error)")
                return
            }
            if let value = response.result.value as? [String: Any]{
                print(value)
                do {
                let decoder = JSONDecoder()
               self.eoHome = try decoder.decode([EOHome].self, from: JSONSerialization.data(withJSONObject: value["data"] as? [[String:Any]], options: []))
                    //print(responseData)
                   // self.tableView.reloadData()
                } catch let err {
                    print(err)
                }
//                if let books = value["Books"] as? [[String:Any]]{
//                    for item in books {
//                        if let bookDetails = EOBookModel(dict: item) {
//                            self.eoModal.append(bookDetails)
//
//                        }
//
//                    }
//                    self.collectionView.reloadData()
//                }
                self.tableView.reloadData()
            }
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let contentView = Bundle.main.loadNibNamed("FooterBar", owner: self, options: nil)?[0] as! UIView
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView.frame = footerView.bounds
        self.footerView.addSubview(contentView)
    }
    @IBAction func homeBtnAction(_ sender:UIButton){
        
        
//        SideMenuNavigationC.menuFadeStatusBar = false
        
        let menu = storyboard?.instantiateViewController(withIdentifier: "MenuBar") as! SideMenuNavigationController
        //SideMenuManager.default.menuWidth = self.view.frame.width - 80
        SideMenuManager.default.leftMenuNavigationController = menu
        
        SideMenuManager.default.addPanGestureToPresent(toView: navigationController!.navigationBar)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: self.view)
        self.present(menu, animated: true, completion: nil)
//SideMenuManager.default.leftMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "sideMenu") as? SideMenuNavigationController
//        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        //SideMenuManager.default.addPanGestureToPresent(toView: navigationController!.navigationBar)
        //SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view)
//      let menu = storyboard?.instantiateViewController(withIdentifier: "SideMenuTableViewController") as! SideMenuTableViewController
//    let leftMenuNavigationController = SideMenuNavigationController(rootViewController: menu)
//      //SideMenuManager.default.leftMenuNavigationController = leftMenuNavigationController
////        SideMenuManager.default.p = SideMenuManager.PresentDirection.left
//        present(leftMenuNavigationController, animated: true, completion: nil)

    }
    
}
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0){
            return 1
        }else{
            return eoHome.count
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.section == 0){
            return 150
        }else if (indexPath.section == 1){
            return 300
        }
        else if (indexPath.section == 2){
            return 300
        }else{
            return 200
        }
    }
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        if (indexPath.section == 0){
//            return 150
//        }else if (indexPath.section == 1){
//            return 300
//        }
//        else if (indexPath.section == 2){
//            return 300
//        }else{
//            return 200
//        }
//    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        

        if (indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeGroupTableViewCell", for: indexPath) as! HomeGroupTableViewCell
           // cell.viewAllView.addGestureRecognizer(<#T##gestureRecognizer: UIGestureRecognizer##UIGestureRecognizer#>)
            let tapped:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector(("tapAction:")))
            tapped.numberOfTapsRequired = 1
            cell.viewAllView.addGestureRecognizer(tapped)
            cell.data = eoGroupArray
            cell.collectionView.reloadData()
            return cell

        }else if (eoHome[indexPath.row].type == "poll"){
            let cell = tableView.dequeueReusableCell(withIdentifier: "PollTableViewCell", for: indexPath) as! PollTableViewCell
            let obj = eoHome[indexPath.row]
            if let eoOption = (obj.poll_options) {
                switch eoOption.count {
                case 0 :
                    cell.stackView1.isHidden = true
                    cell.stackView2.isHidden = true
                    cell.stackView3.isHidden = true
                    cell.stackView4.isHidden = true

                    
                case 1:
                    cell.opt1.text = eoOption[0].option
//                    cell.opt1.isHidden = false
//                    cell.opt2.isHidden = true
//                    cell.opt3.isHidden = true
//                    cell.opt4.isHidden = true
                    cell.stackView1.isHidden = false
                    cell.stackView2.isHidden = true
                    cell.stackView3.isHidden = true
                    cell.stackView4.isHidden = true
                case 2:
                    cell.opt1.text = eoOption[0].option
                    cell.opt2.text = eoOption[1].option
//                    cell.opt1.isHidden = false
//                    cell.opt2.isHidden = false
//                    cell.opt3.isHidden = true
//                    cell.opt4.isHidden = true
                    cell.stackView1.isHidden = false
                    cell.stackView2.isHidden = false
                    cell.stackView3.isHidden = true
                    cell.stackView4.isHidden = true
                case 3:
                    cell.opt1.text = eoOption[0].option
                    cell.opt2.text = eoOption[1].option
                    cell.opt3.text = eoOption[2].option
//                    cell.opt1.isHidden = false
//                    cell.opt2.isHidden = false
//                    cell.opt3.isHidden = false
//                    cell.opt4.isHidden = true
                    cell.stackView1.isHidden = false
                    cell.stackView2.isHidden = false
                    cell.stackView3.isHidden = false
                    cell.stackView4.isHidden = true
                case 4:
                    cell.opt1.text = eoOption[0].option
                    cell.opt2.text = eoOption[1].option
                    cell.opt3.text = eoOption[2].option
                    cell.opt4.text = eoOption[3].option
//                    cell.opt1.isHidden = false
//                    cell.opt2.isHidden = false
//                    cell.opt3.isHidden = false
//                    cell.opt4.isHidden = false
                    cell.stackView1.isHidden = false
                    cell.stackView2.isHidden = false
                    cell.stackView3.isHidden = false
                    cell.stackView4.isHidden = false
                default:
//                    cell.opt1.isHidden = true
//                    cell.opt2.isHidden = true
//                    cell.opt3.isHidden = true
//                    cell.opt4.isHidden = true
                    cell.stackView1.isHidden = true
                    cell.stackView2.isHidden = true
                    cell.stackView3.isHidden = true
                    cell.stackView4.isHidden = true
                    
                }
            }
            
            cell.name.text = obj.name
            cell.post.text = obj.post

            
            return cell
            
        }else if (eoHome[indexPath.row].type == "advertisement"){
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeOfferTableViewCell", for: indexPath) as! HomeOfferTableViewCell
            let obj = eoHome[indexPath.row]

            if obj.add_banner != "" {
                
                Alamofire.request(obj.add_banner ?? "").responseImage { (image) in
                    if let image = image.result.value {
                        cell.advertiseImage.image = image
                        
                    }
                    
                }
              //  print(obj.add_title)
            }
            return cell

        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeConnectTableViewCell", for: indexPath) as! HomeConnectTableViewCell
            let obj = eoHome[indexPath.row]
            cell.date.text = obj.created_date
            cell.name.text = obj.name
            cell.post.text = obj.post
            cell.type.text = obj.type
            
            obj.post_like == 0 ? (cell.like.isHidden = true):(cell.like.text = "\(obj.post_like ?? 0)")
            obj.comment == 0 ? (cell.comment.isHidden = true): (cell.comment.text = "\(obj.comment ?? 0)")

            //cell.comment.text = "\(obj.comment ?? 0)"
            cell.data = eoHome[indexPath.row].post_images
            if obj.user_pic != "" {
                
                Alamofire.request(obj.user_pic ?? "").responseImage { (image) in
                    if let image = image.result.value {
                        cell.img.image = image
                        
                    }
                    
                }
            }
            return cell
            
        }
    }
    func appDelegate () -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
}
