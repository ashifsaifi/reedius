//
//  ExploreViewController.swift
//  Reedius
//
//  Created by Jyoti Negi on 10/12/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit

class ExploreViewController: UIViewController {
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var  collectionView:UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate().controller = self
        collectionView.delegate = self
        collectionView.dataSource = self

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let contentView = Bundle.main.loadNibNamed("FooterBar", owner: self, options: nil)?[0] as! UIView
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView.frame = footerView.bounds
        self.footerView.addSubview(contentView)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func appDelegate () -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
}
extension ExploreViewController:UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 30
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExploreCollectionViewCell", for: indexPath) as! ExploreCollectionViewCell
        return cell
    }
    
    
}
