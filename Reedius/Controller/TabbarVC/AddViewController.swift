//
//  AddViewController.swift
//  Reedius
//
//  Created by Jyoti Negi on 10/12/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit

class AddViewController: UIViewController {
    @IBOutlet weak var footerView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate().controller = self

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        let contentView = Bundle.main.loadNibNamed("FooterBar", owner: self, options: nil)?[0] as! UIView
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView.frame = footerView.bounds
        self.footerView.addSubview(contentView)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func appDelegate () -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
}
