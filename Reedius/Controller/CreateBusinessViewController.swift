//
//  CreateBusinessViewController.swift
//  Reedius
//
//  Created by Jyoti Negi on 06/12/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit

class CreateBusinessViewController: UIViewController {
    @IBOutlet weak var topView:CustomView!
    @IBOutlet weak var bottomView:CustomView!

    @IBOutlet weak var nameView:CustomView!
    @IBOutlet weak var categoryView:CustomView!

    @IBOutlet weak var addressView:CustomView!
    @IBOutlet weak var townView:CustomView!
    @IBOutlet weak var pinView:CustomView!
    @IBOutlet weak var stateView:CustomView!
    @IBOutlet weak var landmarkView:CustomView!


    override func viewDidLoad() {
        super.viewDidLoad()
        screenSetUp()
        // Do any additional setup after loading the view.
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func screenSetUp(){
        topView.shadowFill()
        bottomView.shadowFill()
        nameView.layer.borderColor = UIColor.lightGray.cgColor
        nameView.layer.borderWidth = 1
        categoryView.layer.borderColor = UIColor.lightGray.cgColor
        categoryView.layer.borderWidth = 1
        addressView.layer.borderColor = UIColor.lightGray.cgColor
        addressView.layer.borderWidth = 1
        townView.layer.borderColor = UIColor.lightGray.cgColor
        townView.layer.borderWidth = 1
        pinView.layer.borderColor = UIColor.lightGray.cgColor
        pinView.layer.borderWidth = 1
        stateView.layer.borderColor = UIColor.lightGray.cgColor
        stateView.layer.borderWidth = 1
        landmarkView.layer.borderColor = UIColor.lightGray.cgColor
        landmarkView.layer.borderWidth = 1
        
    }

}
