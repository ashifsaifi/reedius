//
//  CreateEventsViewController.swift
//  Reedius
//
//  Created by Jyoti Negi on 06/12/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit

class CreateEventsViewController: UIViewController {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var contentView:CustomView!
    @IBOutlet weak var photoView:CustomView!
    @IBOutlet weak var eventTitleView:CustomView!
    @IBOutlet weak var detailsView:CustomView!
    @IBOutlet weak var startDateView:CustomView!
    @IBOutlet weak var endDateView:CustomView!
    @IBOutlet weak var startTimeView:CustomView!
    @IBOutlet weak var endTimeView:CustomView!
    @IBOutlet weak var locationView:CustomView!
    @IBOutlet weak var hostInfo:CustomView!
    
    var imagePicker: ImagePicker!

    override func viewDidLoad() {
        super.viewDidLoad()
        screenSetUp()
        self.imagePicker = ImagePicker(presentationController: self, delegate: self  )
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.showImagePicker(_:)))
        photoView.addGestureRecognizer(tap)
        
    }
    @objc func showImagePicker(_ sender: UITapGestureRecognizer) {
        self.imagePicker.present(from: photoView)
    }
    
    func screenSetUp(){
        contentView.shadowFill()
        photoView.shadowFill()
        photoView.layer.borderColor = UIColor.lightGray.cgColor
        photoView.layer.borderWidth = 1
        eventTitleView.layer.borderColor = UIColor.lightGray.cgColor
        eventTitleView.layer.borderWidth = 1
        detailsView.layer.borderColor = UIColor.lightGray.cgColor
        detailsView.layer.borderWidth = 1
        startDateView.layer.borderColor = UIColor.lightGray.cgColor
        startDateView.layer.borderWidth = 1
        endDateView.layer.borderColor = UIColor.lightGray.cgColor
        endDateView.layer.borderWidth = 1
        startTimeView.layer.borderColor = UIColor.lightGray.cgColor
        startTimeView.layer.borderWidth = 1
        endTimeView.layer.borderColor = UIColor.lightGray.cgColor
        endTimeView.layer.borderWidth = 1
        locationView.layer.borderColor = UIColor.lightGray.cgColor
        locationView.layer.borderWidth = 1
        hostInfo.layer.borderColor = UIColor.lightGray.cgColor
        hostInfo.layer.borderWidth = 1
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
extension CreateEventsViewController: ImagePickerDelegate {
    
    func didSelect(image: UIImage?) {
        self.img.image = image
    }
}
