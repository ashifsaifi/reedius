//
//  EventsViewController.swift
//  Reedius
//
//  Created by Jyoti Negi on 10/12/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit

class EventsViewController: UIViewController {

    @IBOutlet weak var tapView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tapView.addGestureRecognizer(tapGesture)
    }
    @objc func handleTap( sender: UITapGestureRecognizer){
    AppHelper.getInstance.pushViewController(identifier: "CreateEventsViewController", controller: self, animation: true)
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
extension EventsViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "EventsTableViewCell", for: indexPath) as! EventsTableViewCell
        return cell
    }
    
    
}
