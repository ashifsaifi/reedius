//
//  GoogleLogin.swift
//  SocialLogin
//
//  Created by Harsh Gangwar on 22/03/19.
//  Copyright © 2019 Harsh Gangwar. All rights reserved.
//

import UIKit
import GoogleSignIn

class GoogleLogin: NSObject {
    
    var actionHandler: ((_ user: GIDGoogleUser)->Void)?
    
    /*!
     * @discussion Method to do Google Login by calling it delegates
     * @param handler: - an escaping closure with parameter GIDGoogleUser
     * @return void
     */
    func googleVerification(handler: @escaping ((GIDGoogleUser))->Void){
        actionHandler                          = handler
        GIDSignIn.sharedInstance().delegate    = self
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.signIn()
    }
}
 // MARK: - Extension of Google Login for GIDSignInDelegate and  GIDSignInUIDelegate

extension GoogleLogin: GIDSignInDelegate{
   
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            signIn.signOut()
            actionHandler!(user)
        }
    }
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        
    }
}
