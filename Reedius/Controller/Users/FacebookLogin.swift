//
//  FacebookLogin.swift
//  SocialLogin
//
//  Created by Harsh Gangwar on 20/03/19.
//  Copyright © 2019 Harsh Gangwar. All rights reserved.
//

import UIKit
//import FBSDKShareKit
import FBSDKLoginKit
import FBSDKCoreKit
class FacebookLogin: NSObject {
    
     // MARK: - Properties
    
    var actionHandler: ((_ faceBookData: NSDictionary) -> Void)?
    
    /*!
     * @discussion Method to do login and create connection with facebook
     * @param handler , an escaping closure with
             @param NSDictionary
     * @return void
     */
    
    func fbLogin(controller: UIViewController, handler: @escaping ((NSDictionary)) -> Void){
        let loginManager = LoginManager()
        if AccessToken.current == nil {
            loginManager.logIn(permissions: ["public_profile","email"], from: controller, handler: {(result:LoginManagerLoginResult!, error:NSError!) -> Void in
        if error != nil {
          }else if result.isCancelled {
             print("Login process is cancelled By user")
          }
                } as? LoginManagerLoginResultBlock )
    }else {
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email,picture"]).start(completionHandler: { (connection, result, error) -> Void in
            if (error == nil){
                let fbDetails = result as! NSDictionary
                handler(fbDetails)
                }else {
                print(error?.localizedDescription ?? "Not found")
            }
    })
  }
}
    /*!
     * @discussion Method to share image on the Facebook
     * @param -
     * @return -
     */
//    func facebookImageShare(controller: UIViewController){
//        let uploadPhoto             = FBSDKSharePhoto()
//        let shareContent            = FBSDKSharePhotoContent()
//        uploadPhoto.image           = #imageLiteral(resourceName: "logout")
//        uploadPhoto.isUserGenerated = true
//        shareContent.photos         = [uploadPhoto]
//        FBSDKShareDialog.show(from: controller, with: shareContent, delegate: nil)
//    }
//

  
   
}

