//
//  LoginViewController.swift
//  Reedius
//
//  Created by Sushant Jugran on 22/11/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
class LoginViewController: UIViewController {

    //=================
    //MARK:- IBOutlets
    
    @IBOutlet weak var emailView         : CustomView!
    @IBOutlet weak var passwordView      : CustomView!
    @IBOutlet weak var emailTextField    : UITextField!
    @IBOutlet weak var passwordTextField : UITextField!
    
    //===================
    //MARK:- Life Cycle
    
    
    // Add this to the body
    
    let facebook = FacebookLogin()
    let google = GoogleLogin()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        screenSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    //=====================
    //MARK:- IBACtions
    
    @IBAction func signinButtonTapped(_ sender: UIButton) {
        guard !emailTextField.isTextFieldEmpty(controller: self, message: "Email cannot be Empty") else {
            return
        }
        guard !passwordTextField.isTextFieldEmpty(controller: self, message: "Password cannot be Empty") else {
            return
        }
        loginApiHit()
        
    }
    
    //facebook login
    @IBAction func facebookButtonTapped(_ sender: Any) {
        facebook.fbLogin( controller:self,handler: {(fbDetails) in
          fbDetails["name"] as? String
            fbDetails["email"] as? String
          
        })
    }
    
    
    //google Login
    @IBAction func googleButtonTapped(_ sender: Any) {
        google.googleVerification { (userDetails) in
           userDetails.profile.name
         userDetails.profile.email
//            if userDetails.profile.hasImage{
//                if let imageURL = userDetails.profile.imageURL(withDimension: 120)?.absoluteURL {
//                    print(imageURL)
//                    let url   = URL(string: imageURL.absoluteString)
//                    let data  = NSData(contentsOf: url!)
//                    let image = UIImage(data: data! as Data)
////                    self.userProfilePicture.image = image
////                    self.facebookShareButton.isHidden = false
////                    self.instagramShareButton.isHidden = false
////                    self.titleLabel.text = "Google"
////                    self.logoutButton.isHidden = false
////                    self.hideLoginButtons(value: true)
//                }
//            }
        }
    }
    
    
    @IBAction func registerButtonTapped(_ sender: UIButton) {
        //Push Registeration view Controller
        AppHelper.getInstance.pushViewController(identifier: "SignUpViewController", controller: self, animation: true)
    }
    
}

//===========================
//MARK:- Private Methods
extension LoginViewController {

    private func screenSetup(){
        self.emailView.shadowFill()
        self.passwordView.shadowFill()
    }
    
    private func saveUserData(userData : LoginModel) {
    
        if userData.addressLine1H != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.addressLine1H, key: AppConstansts.userDefaults.addressLine1h)
        }
        if userData.addressLine1W != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.addressLine1W, key: AppConstansts.userDefaults.addressLine1W)
        }
        if userData.addressLine2H != ""{
            AppHelper.getInstance.setValueForUserDefault(value: userData.addressLine2H, key: AppConstansts.userDefaults.addressLine2H)
        }
        if userData.addressLine2W != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.addressLine2W, key: AppConstansts.userDefaults.addressLine2W)
        }
        if userData.age != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.age, key: AppConstansts.userDefaults.age)
        }
        if userData.cityH != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.cityH, key: AppConstansts.userDefaults.cityH)
        }
        if userData.cityW != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.cityW, key: AppConstansts.userDefaults.cityW)
        }
        if userData.countryH != "" {
             AppHelper.getInstance.setValueForUserDefault(value: userData.countryH, key: AppConstansts.userDefaults.countryH)
        }
        if userData.countryW != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.countryW, key: AppConstansts.userDefaults.countryW)
        }
        if userData.createdDate != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.createdDate, key: AppConstansts.userDefaults.createdDate)
        }
        if userData.email != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.email, key: AppConstansts.userDefaults.email)
        }
        if userData.groupId != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.groupId, key: AppConstansts.userDefaults.groupId)
        }
        if userData.latH != "" {
             AppHelper.getInstance.setValueForUserDefault(value: userData.latH, key: AppConstansts.userDefaults.latH)
        }
        if userData.latW != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.latW, key: AppConstansts.userDefaults.latW)
        }
        if userData.longH != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.longH, key: AppConstansts.userDefaults.longH)
        }
        if userData.longW != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.longW, key: AppConstansts.userDefaults.longW)
        }
        if userData.mobile != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.mobile, key: AppConstansts.userDefaults.mobile)
        }
        if userData.name != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.name, key: AppConstansts.userDefaults.name)
        }
        if userData.otp != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.otp, key: AppConstansts.userDefaults.otp)
        }
        if userData.password != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.password , key: AppConstansts.userDefaults.password)
        }
        if userData.role != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.role, key: AppConstansts.userDefaults.role)
        }
        if userData.sex != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.sex, key: AppConstansts.userDefaults.sex)
        }
        if userData.socialType != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.socialType, key: AppConstansts.userDefaults.socialType)
        }
        if userData.socilaToken != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.socilaToken, key: AppConstansts.userDefaults.socialToken)
        }
        if userData.stateH != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.stateH, key: AppConstansts.userDefaults.stateH)
        }
        if userData.stateW != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.stateW, key: AppConstansts.userDefaults.stateW)
        }
        if userData.userId != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.userId, key: AppConstansts.userDefaults.userId)
        }
        if userData.userimg != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.userimg, key: AppConstansts.userDefaults.userImg)
        }
        if userData.waitOtp != "" {
            AppHelper.getInstance.setValueForUserDefault(value: userData.waitOtp, key: AppConstansts.userDefaults.waitOtp)
        }
        self.view.showToast(message: "Login Successfull", duration: 3.0)
    }
}

//============================
//MARK:- Web Api Methods

extension LoginViewController {
    func loginApiHit(){
        if Connectivity.isConnectedToInternet(){
            AppHelper.getInstance.showProgressIndicator(view: self.view)
            WebManager.getInstance.requestWithPost(baseUrl: AppConstansts.AppUrls.baseUrl, endUrl: AppConstansts.AppUrls.loginApp, parameters: [AppConstansts.loginParameters.emailId : emailTextField.text ?? "" ,AppConstansts.loginParameters.password : passwordTextField.text ?? ""], onCompletion: { (jsonData) in
                AppHelper.getInstance.hideProgressIndicator(view: self.view)
                if jsonData["status_code"] as? Int == 200 {
                    if let userData = LoginModel.init(dict: jsonData["data"] as! [String : Any]) {
                        AppHelper.getInstance.setUserLoggedIn(value: true)
                        self.saveUserData(userData: userData)
                    }
                } else {
                    if (jsonData[AppConstansts.dataKeys.message] as! String).lowercased() == "not match" {
                        AppHelper.getInstance.showAlert(controller: self, message: "Invalid login Credentials")
                    } else {
                        AppHelper.getInstance.showAlert(controller: self, message: jsonData[AppConstansts.dataKeys.message] as! String)
                    }
                }
                print(jsonData)
            }) { (error) in
                AppHelper.getInstance.hideProgressIndicator(view: self.view)
                AppHelper.getInstance.showAlert(controller: self, message: "Oops! Something went wrong")
            }
        } else {
            AppHelper.getInstance.showAlert(controller: self, message: AppConstansts.ErrorMessages.internetConnection)
            //Internet Connection Error
        }
    }
}

