//
//  SignUpViewController.swift
//  Reedius
//
//  Created by Sushant Jugran on 22/11/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    //=======================
    //MARK:- IBoutlets
    
    @IBOutlet weak var ageTextField              : UITextField!
    @IBOutlet weak var nameTextField             : UITextField!
    @IBOutlet weak var ageCustomView             : CustomView!
    @IBOutlet weak var emailTextField            : UITextField!
    @IBOutlet weak var nameCustomView            : CustomView!
    @IBOutlet weak var genderTextField           : UITextField!
    @IBOutlet weak var maleRadioButton           : UIButton!
    @IBOutlet weak var emailCustomView           : CustomView!
    @IBOutlet weak var genderCustomView          : CustomView!
    @IBOutlet weak var passwordTextField         : UITextField!
    @IBOutlet weak var femaleRadioButton         : UIButton!
    @IBOutlet weak var passwordCustomView        : CustomView!
    @IBOutlet weak var mobileNumberTextField     : UITextField!
    @IBOutlet weak var mobileNumberCustomView    : CustomView!
    @IBOutlet weak var confirmPasswordTextField  : UITextField!
    @IBOutlet weak var confirmPasswordCustomView : CustomView!
    
    
    //===============================
    //MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        screenSetup()
    }
    
    //=========================
    //MARK:- IBACtions

    @IBAction func registerButtonTapped(_ sender: Any) {
        guard !nameTextField.isTextFieldEmpty(controller: self, message: "Name cannot be Empty") else {
            return
        }
        guard  !ageTextField.isTextFieldEmpty(controller: self, message: "Age cannot be empty") else {
            return
        }
        guard !emailTextField.isTextFieldEmpty(controller: self, message: "Email cannot be empty") else {
            return
        }
        guard !mobileNumberTextField.isTextFieldEmpty(controller: self, message: "Mobile Number cannot be empty") else {
            return
        }
        guard !passwordTextField.isTextFieldEmpty(controller: self, message: "Password cannot be empty") else {
            return
        }
        guard !genderTextField.isTextFieldEmpty(controller: self, message: "Gender cannot be empty") else {
            return
        }
        if passwordTextField.text ?? "" != confirmPasswordTextField.text ?? ""{
            AppHelper.getInstance.showAlert(controller: self, message: "password and confirm password must be same")
            return
        }
        hitSignupApi()
        
        
    }
    
    @IBAction func femaleButtonTapped(_ sender: Any) {
        
        if femaleRadioButton.image(for: .normal) == #imageLiteral(resourceName: "radio") {
            femaleRadioButton.setImage(#imageLiteral(resourceName: "radio_selected"), for: .normal)
            maleRadioButton.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
            genderTextField.text = "Female"
        } else {
            femaleRadioButton.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
            genderTextField.text = ""
        }
    }
    
    @IBAction func maleButtonTapped(_ sender: Any) {
        
        if maleRadioButton.image(for: .normal) == #imageLiteral(resourceName: "radio") {
            maleRadioButton.setImage(#imageLiteral(resourceName: "radio_selected"), for: .normal)
            genderTextField.text = "Male"
            femaleRadioButton.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
        } else {
            maleRadioButton.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
            genderTextField.text = ""
        }
    }
    @IBAction func OnLoginTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

//===========================
//MARK:- Private Methods
extension SignUpViewController {
    
    private func screenSetup() {
        nameCustomView.shadowFill()
        ageCustomView.shadowFill()
        emailCustomView.shadowFill()
        genderCustomView.shadowFill()
        passwordCustomView.shadowFill()
        confirmPasswordCustomView.shadowFill()
        mobileNumberCustomView.shadowFill()
    }
}

//=============================
//MARK:- Web Services Methods

extension SignUpViewController {
    private func hitSignupApi(){
        if Connectivity.isConnectedToInternet() {
            
            AppHelper.getInstance.showProgressIndicator(view: self.view)
            let params : [String:String] = [AppConstansts.signupParameters.email : emailTextField.text ?? "",AppConstansts.signupParameters.age : ageTextField.text ?? "", AppConstansts.signupParameters.gender : genderTextField.text ?? "",AppConstansts.signupParameters.mobile : mobileNumberTextField.text ?? "",AppConstansts.signupParameters.name : nameTextField.text ?? "",AppConstansts.signupParameters.password : passwordTextField.text ?? ""]
            WebManager.getInstance.requestWithPost(baseUrl: AppConstansts.AppUrls.baseUrl, endUrl: AppConstansts.AppUrls.signup, parameters: params, onCompletion: { (responseData) in
                print(responseData)
                if responseData[AppConstansts.dataKeys.statusCode] as? Int == 200 {
                    AppHelper.getInstance.hideProgressIndicator(view: self.view)
                    if let userData = responseData[AppConstansts.dataKeys.data] as? [String:Any] {
                        if let userId = userData[AppConstansts.userDefaults.userId] as? Int {
                            let destVc = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                            destVc.data = "\(userId)"
                            self.navigationController?.pushViewController(destVc, animated: true)
                        }
                    }
                } else {
                    AppHelper.getInstance.hideProgressIndicator(view: self.view)
                    AppHelper.getInstance.showAlert(controller: self, message: responseData[AppConstansts.dataKeys.message] as! String)
                }
            }) { (error) in
                AppHelper.getInstance.hideProgressIndicator(view: self.view)
                AppHelper.getInstance.showAlert(controller: self, message: "Oops! Something went wrong")
            }
        } else {
            AppHelper.getInstance.showAlert(controller: self, message: AppConstansts.ErrorMessages.internetConnection)
        }
    }
}
