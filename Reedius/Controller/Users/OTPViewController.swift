//
//  PhoneNumberViewController.swift
//  Reedius
//
//  Created by Sushant Jugran on 26/11/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit

class OTPViewController: UIViewController {

    var data = ""
    //====================
    //MARK:- IBOutlets
    
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var otpCustomView    : CustomView!
    @IBOutlet weak var otpTextField     : UITextField!
    
    //====================
    //MARK:- Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
       screenSetup()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        let contentView = Bundle.main.loadNibNamed("FooterBar", owner: self, options: nil)?[0] as! UIView
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView.frame = footerView.bounds
        self.footerView.addSubview(contentView)
    }
    
    //======================
    //MARK:- IBActions
    
    @IBAction func onBackButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSubmitButtonTapped(_ sender: Any) {
        guard !otpTextField.isTextFieldEmpty(controller: self, message: "Phone Number cannot be empty") else {
            return
        }
        hitValidateOTP()
    }
}

//=====================
//MARK:- Private Methods

extension OTPViewController {
    private func screenSetup() {
        otpCustomView.shadowFill()
    }
}

//===============================
//MARK:- Web Services Methods

extension OTPViewController {
    func hitValidateOTP() {
        if Connectivity.isConnectedToInternet() {
            AppHelper.getInstance.showProgressIndicator(view: self.view)
            let params : [String:String] = [AppConstansts.otpParameters.otp : otpTextField.text ?? "" , AppConstansts.otpParameters.userId : self.data]
            WebManager.getInstance.requestWithPost(baseUrl: AppConstansts.AppUrls.baseUrl, endUrl: AppConstansts.AppUrls.verifyOtp, parameters: params
                , onCompletion: { (responseData) in
                    if responseData[AppConstansts.dataKeys.statusCode] as? Int == 200 {
                        AppHelper.getInstance.hideProgressIndicator(view: self.view)
                        //Push Home View Controller
                        //Signup Successfull
                    } else {
                        AppHelper.getInstance.hideProgressIndicator(view: self.view)
                        AppHelper.getInstance.showAlert(controller: self, message: responseData[AppConstansts.dataKeys.message] as! String)
                    }
                    print(responseData)
            }) { (nil) in
                AppHelper.getInstance.showProgressIndicator(view: self.view)
                AppHelper.getInstance.showAlert(controller: self, message: "")
            }
        } else {
            AppHelper.getInstance.showAlert(controller: self, message: AppConstansts.ErrorMessages.internetConnection)
        }
    }
}
