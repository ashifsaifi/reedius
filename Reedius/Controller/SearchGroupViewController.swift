//
//  SearchGroupViewController.swift
//  Reedius
//
//  Created by Sushant Jugran on 23/11/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit

class SearchGroupViewController: UIViewController {

    //========================
    //MARK:- IBOutlets
    
    @IBOutlet weak var GroupCategoriesCollectionView : UICollectionView!
    @IBOutlet weak var searchGroupTextField          : UITextField!
    @IBOutlet weak var groupTableView                : UITableView!
    
    //=====================
    //MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenSetup()
    }
    
    //======================
    //MARK:- IBActions
 
     @IBAction func onBackButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
     }
}

//==========================
//MARK:- Private Methods
extension SearchGroupViewController {
    func screenSetup() {
        searchGroupTextField.delegate            = self
        groupTableView.delegate                  = self
        groupTableView.dataSource                = self
        GroupCategoriesCollectionView.delegate   = self
        GroupCategoriesCollectionView.dataSource = self
    }
}

//===============================
//MARK:- Text Field Delegates

extension SearchGroupViewController : UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch  textField {
        case  searchGroupTextField : if textField.text?.lowercased() == "search group" {
                                            textField.text = ""
                                        }
            
        default:
            return true
        }
        return true
    }
}


extension SearchGroupViewController : UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GroupCategoriesCollectionViewCell", for: indexPath) as! GroupCategoriesCollectionViewCell
        cell.categoryImageView.layer.cornerRadius = cell.categoryImageView.frame.width / 2
        return cell
    }
}
extension SearchGroupViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell")!
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupSuggestionsTableViewCell") as! GroupSuggestionsTableViewCell
        cell.categoryImageView.layer.cornerRadius = cell.categoryImageView.frame.width/2
        return cell
    }
}
