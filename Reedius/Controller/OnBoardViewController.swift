//
//  OnBoardViewController.swift
//  Reedius
//
//  Created by Jyoti Negi on 03/12/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit

class OnBoardViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var prevBtn:UIButton!
    @IBOutlet weak var nextBtn:UIButton!
    
    var imgArray = ["1","2","3","4","5","6","7","8","9"]
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        self.pageControl.numberOfPages = imgArray.count
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

                if (pageControl.currentPage == 0){
                    self.prevBtn.isHidden = true
                }else{
                    self.prevBtn.isHidden = false
        
                }
      
//        if(imgArray.first == "1"){
//            self.prevBtn.isHidden = true
//
//        }else{
//            self.prevBtn.isHidden = false
//
//        }
        
    }
    
    
    
    @IBAction func skipAction(_ sender:UIButton){
        AppHelper.getInstance.pushViewController(identifier: "LoginViewController", controller: self, animation: true)
    }
    @IBAction func prevAction(_ sender:UIButton){
        let collectionBounds = self.collectionView.bounds
        let contentOffset = CGFloat(floor(self.collectionView.contentOffset.x - collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset )
    }
    @IBAction func nextAction(_ sender:UIButton){
        let collectionBounds = self.collectionView.bounds
        let contentOffset = CGFloat(floor(self.collectionView.contentOffset.x + collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
    }
    func moveCollectionToFrame(contentOffset : CGFloat) {
        let frame: CGRect = CGRect(x : contentOffset ,y : self.collectionView.contentOffset.y ,width : self.collectionView.frame.width,height : self.collectionView.frame.height)
        self.collectionView.scrollRectToVisible(frame, animated: true)
    }
    
}
extension OnBoardViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OnBoardCollectionViewCell", for: indexPath) as! OnBoardCollectionViewCell
        cell.img.image = UIImage(named: imgArray[indexPath.item])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.view.frame.width, height: self.view.frame.height)
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let witdh = scrollView.frame.width - (scrollView.contentInset.left*2)
        let index = scrollView.contentOffset.x / witdh
        let roundedIndex = round(index)
        self.pageControl?.currentPage = Int(roundedIndex)
        if (pageControl.currentPage == 0 ){
            self.prevBtn.isHidden = true
        }else if (pageControl.currentPage == 8){
            self.nextBtn.isHidden = true
        }else{
            self.prevBtn.isHidden = false
            self.nextBtn.isHidden = false

        }
    }
    
}
