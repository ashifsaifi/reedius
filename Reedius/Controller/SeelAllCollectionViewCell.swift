//
//  SeelAllCollectionViewCell.swift
//  Reedius
//
//  Created by Jyoti Negi on 11/12/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit

class SeelAllCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var img:UIImageView!
    @IBOutlet weak var name:UILabel!
    @IBOutlet weak var members:UILabel!
    @IBOutlet weak var cellWidth: NSLayoutConstraint!
    
}
