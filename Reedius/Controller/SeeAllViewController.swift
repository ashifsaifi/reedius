//
//  SeeAllViewController.swift
//  Reedius
//
//  Created by Jyoti Negi on 11/12/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class SeeAllViewController: UIViewController {
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    
    var allGroupArray: [EOAllGroup]?
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        flowLayout.itemSize = CGSize(width: (self.view.frame.width/3) - 20,height: 150)

    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension SeeAllViewController : UICollectionViewDataSource,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allGroupArray?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((self.view.frame.width ) / 3) - 20
        return CGSize(width: width, height: 145)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SeelAllCollectionViewCell" , for: indexPath) as! SeelAllCollectionViewCell
        let obj = allGroupArray?[indexPath.row]
        cell.name.text = obj?.group_name
//        var width =  ((self.view.frame.width ) / 3) - 20
//
//        cell.img.layer.cornerRadius = width/2
        if obj?.img != "" {
            
            Alamofire.request(obj?.img ?? "").responseImage { (image) in
                if let image = image.result.value {
                    cell.img.image = image
                    
                }
                
            }
        }
        
        return cell
        
    }
    
    
}
