//
//  SplashViewController.swift
//  Reedius
//
//  Created by Jyoti Negi on 03/12/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            AppHelper.getInstance.pushViewController(identifier: "OnBoardViewController", controller: self, animation: true)
        }

    }
}
