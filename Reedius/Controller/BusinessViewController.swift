//
//  BusinessViewController.swift
//  Reedius
//
//  Created by Jyoti Negi on 05/12/19.
//  Copyright © 2019 Sushant Jugran. All rights reserved.
//

import UIKit

class BusinessViewController: UIViewController {
    @IBOutlet weak var collectionView:UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

}
extension BusinessViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BusinessCollectionViewCell", for: indexPath) as! BusinessCollectionViewCell
        return cell
    }
    
    
}
